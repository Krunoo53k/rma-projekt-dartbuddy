package hr.ferit.krunoslapetrik.dartbuddy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ScreenSlidePagePlayersFragment extends Fragment {
    private static final String BUNDLE_MESSAGE = "message";

    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private EditText editText;
    private Button addButton;
    private static List<PlayerInfo> data = new ArrayList<>();

    public static ScreenSlidePagePlayersFragment newInstance(String message){
        ScreenSlidePagePlayersFragment fragment=new ScreenSlidePagePlayersFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_second_screen_slide_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpRecyclerView(view);
        setupRecyclerData();
        setUpButtons(view);
    }
    private void setUpRecyclerView(View view){
        recyclerView = view.findViewById(R.id.rView);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void setUpButtons(View view){
        editText=view.findViewById(R.id.editText);
        addButton=view.findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAddPlayer(v);
            }
        });
        }


    private void setupRecyclerData(){
        data.add(new PlayerInfo("Kruno"));
        data.add(new PlayerInfo("Bruno"));
        data.add(new PlayerInfo("Zruno"));
        adapter.addData(data);
    }

    private void onClickAddPlayer(View view){
        String playerName= editText.getText().toString();
        data.add(0,new PlayerInfo(playerName));
        adapter.addNewCell(playerName,0);
    }

    public static List<PlayerInfo> getPlayers(){
        return data;
    }

}

