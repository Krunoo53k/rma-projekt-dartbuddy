package hr.ferit.krunoslapetrik.dartbuddy;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder {

    private TextView tvName;
    public ImageButton delete;

    public NameViewHolder(@NonNull View itemView) {
        super(itemView);
        tvName=itemView.findViewById(R.id.tvName);
        delete=itemView.findViewById(R.id.imButton);
    }

    public void setName(String name){
        tvName.setText(name);
    }

}
