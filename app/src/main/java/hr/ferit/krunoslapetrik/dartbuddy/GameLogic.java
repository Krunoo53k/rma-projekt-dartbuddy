package hr.ferit.krunoslapetrik.dartbuddy;

import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class GameLogic {
    private int winPoints;
    private int turn;
    private int numberOfPlayers;
    private int currentPosition;
    private ArrayList<PlayerInfo> gamePlayerList;

    public GameLogic(){
        this.winPoints=301;
        this.turn=0;
        this.numberOfPlayers=0;
    }

    public void setPlayers(ArrayList<PlayerInfo> playerList){
        gamePlayerList= playerList;
        this.numberOfPlayers=gamePlayerList.size();
    }

    public void startGame(ArrayList<PlayerInfo> players){
        setPlayers(players);
        this.turn=0;
        this.currentPosition=0;
        for(PlayerInfo player : players) player.setScore(0);
    }

    public void addPoints(int amountOfPoints, TextView feedback, TextView currentTurnPlayerName){
        if(this.gamePlayerList==null) {
            feedback.setText("The list is empty.");
        }
        else
        {
            if(turn==3) skipPlayer();
            if(currentPosition>=numberOfPlayers){
                currentPosition=0;
                turn=0;
            }
            setCurrentTurnName(currentTurnPlayerName);
            if(amountOfPoints+gamePlayerList.get(currentPosition).getScore()==winPoints)
            {
                gamePlayerList.get(currentPosition).addScore(amountOfPoints);
                feedback.setText(gamePlayerList.get(currentPosition).getName()+" has won!");
            }
            else if(amountOfPoints+gamePlayerList.get(currentPosition).getScore()>winPoints)skipPlayer();
            else{
                turn++;
                gamePlayerList.get(currentPosition).addScore(amountOfPoints);
                updateLabel(feedback, gamePlayerList.get(currentPosition));
            }


        }
    }

    private void skipPlayer(){
        currentPosition+=1;
        turn=0;
        Log.d("Game", Integer.toString(currentPosition));
    }

    private void updateLabel(TextView feedback, PlayerInfo currentPlayer){
        feedback.setText(currentPlayer.getName()+" has "+currentPlayer.getScore()+" points.");
    }

    public List<PlayerInfo> getPlayers(){
        return this.gamePlayerList;
    }

    public void setCurrentTurnName(TextView name){
        if(turn==2){
            if(currentPosition==numberOfPlayers-1)
                name.setText(gamePlayerList.get(0).getName() + "'s turn");
            else
                name.setText(gamePlayerList.get(currentPosition+1).getName()+"'s turn");
        }
        else
            name.setText(gamePlayerList.get(currentPosition).getName()+"'s turn");
    }

}
