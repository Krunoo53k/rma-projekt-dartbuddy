package hr.ferit.krunoslapetrik.dartbuddy;

public interface ButtonClickListener {
    void onButtonClick(int position);
}
