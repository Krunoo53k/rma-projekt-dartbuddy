package hr.ferit.krunoslapetrik.dartbuddy;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<NameViewHolder>  {

    private List<PlayerInfo> dataList=new ArrayList<>();

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View cellView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cell, parent, false);
       return new NameViewHolder(cellView);
    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder holder, int position) {
        final PlayerInfo current = this.dataList.get(position);
        holder.setName(current.getName());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenSlidePagePlayersFragment.getPlayers().remove(position);
                removeCell(position); }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addData(List<PlayerInfo> data){
        this.dataList.clear();
        this.dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewCell(String name, int position){
        PlayerInfo playerInfo = new PlayerInfo(name);
        if(dataList.size()>=position){
            dataList.add(position,playerInfo);
            notifyItemInserted(position);
        }
    }
    public void removeCell(int position){
        if(dataList.size()>position){
            dataList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position,getItemCount());
        }
    }

    public List<PlayerInfo> getPlayerList(){
        return this.dataList;
    }

}
