package hr.ferit.krunoslapetrik.dartbuddy;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class ScreenSlidePageFragment extends Fragment {
    private static final String BUNDLE_MESSAGE = "message";

    private TextView mMessageTextView;
    private Button btnStart;
    private ImageButton btnCheck;
    private EditText etPoints;
    private GameLogic gameLogic;
    private TextView newTextView;

    public static ScreenSlidePageFragment newInstance(String message){
        ScreenSlidePageFragment fragment=new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        newTextView=view.findViewById(R.id.newTextView);
        mMessageTextView = view.findViewById(R.id.tvMessage);
        btnStart = view.findViewById(R.id.btnStart);
        etPoints=view.findViewById(R.id.etPoints);
        btnCheck=view.findViewById(R.id.btnCheck);
        gameLogic=new GameLogic();
        setUpButtonStart();
        setUpPointsCheck();
    }

    private void setUpButtonStart() {
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<PlayerInfo> players = new ArrayList<>(ScreenSlidePagePlayersFragment.getPlayers());
                for(int i=0;i<players.size();i++){
                    Log.d("Player", players.get(i).getName());
                }
                gameLogic.startGame(players);
                newTextView.setText("Game started!");
                gameLogic.setCurrentTurnName(mMessageTextView);
            }
        });
    }

    private void setUpPointsCheck(){
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!gameLogic.getPlayers().isEmpty() && !etPoints.getText().toString().equals("")) {
                    gameLogic.addPoints(Integer.parseInt(etPoints.getText().toString()), mMessageTextView, newTextView);
                }
            }
        });
    }




}
