package hr.ferit.krunoslapetrik.dartbuddy;

import java.util.ArrayList;

public class PlayerInfo {
    private String name;
    private int score;

    public PlayerInfo(String name){
        this.setName(name);
        this.score=0;
    }

    public PlayerInfo(PlayerInfo playerInfo){
        this.name=playerInfo.getName();
        this.score=playerInfo.getScore();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int points) {
        this.score+=points;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
